﻿using System;
using System.Threading.Tasks;

namespace LessonOTUS19
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            string fileName = "image.png";
            Motivator motivator = new Motivator();
            Console.WriteLine($"File saved here: " + await motivator.CreateMotivator(fileName));
            Console.ReadKey();
        }
    }
}