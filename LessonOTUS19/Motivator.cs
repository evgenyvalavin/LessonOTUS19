﻿using System;
using System.Threading.Tasks;

namespace LessonOTUS19
{
    public class Motivator
    {
        public async Task<string> CreateMotivator(string image)
        {
            BaseImage baseimage = new BaseImage();
            var bitImage = baseimage.CreateBase();

            ColorImage colors = new ColorImage();
            colors.AddColors(bitImage);

            DownloadImage download = new DownloadImage();
            var downloaded = await download.Download(new Uri("https://www.pressa.tv/uploads/posts/2013-09/1379566087_y20-kopiya.jpg"));

            SaveImage saver = new SaveImage();
            string fileName = saver.Save(downloaded, image);
            return fileName;
        }
    }
}