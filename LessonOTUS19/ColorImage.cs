﻿using System.Drawing;

namespace LessonOTUS19
{
    public class ColorImage
    {
        public Graphics AddColors(Bitmap bitmap)
        {
            Graphics flagGraphics = Graphics.FromImage(bitmap);
            int red = 0;
            int white = 11;
            while (white <= 100)
            {
                flagGraphics.FillRectangle(Brushes.Red, 0, red, 200, 10);
                flagGraphics.FillRectangle(Brushes.White, 0, white, 200, 10);
                red += 20;
                white += 20;
            }
            return flagGraphics;
        }
    }
}
