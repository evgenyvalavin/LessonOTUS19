﻿using System;
using System.Net.Http;
using System.Drawing;
using System.Threading.Tasks;

namespace LessonOTUS19
{
    public class DownloadImage
    {
        public async Task<Bitmap> Download(Uri uri)
        {
            Bitmap bitmapImage;
            using HttpClient client = new HttpClient();
            using var response = await client.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            using var inputStream = await response.Content.ReadAsStreamAsync();
            return bitmapImage = new Bitmap(inputStream);
        }
    }
}