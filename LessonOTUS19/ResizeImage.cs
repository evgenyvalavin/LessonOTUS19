﻿using System.Drawing;

namespace LessonOTUS19
{
    public class ResizeImage
    {
        public void Resize(Bitmap bitmap, int xDpi, int yDpi)
        {
            bitmap.SetResolution(xDpi, yDpi);
        }
    }
}