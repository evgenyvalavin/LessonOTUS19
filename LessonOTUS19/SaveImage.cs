﻿using System;
using System.Drawing;
using System.IO;

namespace LessonOTUS19
{
    public class SaveImage
    {
        public string Save(Bitmap bitmap, string fileName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            var bmpNew = new Bitmap(bitmap.Width, bitmap.Height);
            Graphics graphics = Graphics.FromImage(bmpNew);
            graphics.DrawImage(bitmap, new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel);
            bitmap.Dispose();
            bitmap = bmpNew;
            bitmap.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            return path;
        }
    }
}